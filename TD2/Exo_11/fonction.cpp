#include "fonction.h"
#include <iostream>

int main()
{
	//double& d1 = 36;//Une r�f�rence est sur une l value, or 36 n'en n'est pas une (pas d'existance propre).
	double d2 = 36;
	double& ref = d2;//d2 a un espace m�moire, donc possible d'utiliser une r�f�rence (alias de d2).
	ref = 4; //Equivaut � d2 = 4.
	const double d3 = 36; 
	const double& d4 = 36;//Dans ce cas, on va cr�er un espace m�moire comme on a pr�cis� const (pas forc�ment recommendable). 
	const double& d5 = d2; //Une r�f�rence qui garentie qu'on ne modifera pas d2. Marche m�me si d2 n'est pas constante.
	//d5 = 21; //Marche pas : d5 ne permet pas de modifier une valeur.
	const double& d6 = d3; //Une r�f�rence qui garentie qu'on ne modifera pas d3.
	//double& ref2 = d6; //Pas autoris� de faire une conversion const double& -> double&
	double& ref2 = const_cast<double&>(d6); //Autoris�.
	int i = 4;
	//double& d7 = i;//Pas le bon type. Ici double, mais int attendu.
	const double& d8 = i;
	//d8 = 3; //D8 est const, on ne peut pas modifier.
	return 0;
}