#include "fonction.h"
#include <iostream>

void raz(essai* e)
{
	e->n = 0;
	e->x = 0.0;
	return;
}

void raz(essai& e)
{
	e.n = 0;
	e.x = 0.0;
	return;
}

int main()
{
	essai e;
	raz(&e);
	std::cout << e.n << " " << e.x << "\n";
	essai f;
	raz(f); //Pas besoin d'une r�f�rence, l'objet lui m�me marche !
	std::cout << f.n << " " << f.x << "\n";
	return 0;
}