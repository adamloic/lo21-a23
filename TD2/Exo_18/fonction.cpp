#include "fonction.h"
#include <iostream>

int& operation(compte* tab, const std::string& c)
{
	while (tab->id != c)
		tab++;

	return tab->solde; //Possible aussi de retourner int* avec &tab->solde
}

void essai_comptes() 
{
	compte tab[4] = { 
		{"courant", 0},
		{"codevi", 1500 },
		{"epargne", 200 }, 
		{ "cel", 300 } 
	};
	//Si avec des pointeurs, mettre * avant operation pour avoir la valeur point�e, et non une adresse.
	operation(tab, "courant") = 100; //On � une lvalue �quivalant � tab[0]. On fait donc tab[0] = 100
	operation(tab, "codevi") += 100;
	operation(tab, "cel") -= 50;
	for (int i = 0; i < 4; i++) 
		std::cout << tab[i].id << " : " << tab[i].solde << "\n";
}

int main()
{
	essai_comptes();
	return 0;
}