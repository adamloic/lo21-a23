#include "fonction.h"
#include <iostream>

void raz(personne& p)
{
	p.age = 0;
	*p.nom = 0; //Ou sinon = '\0' //Pointeur car tableau.
}
void affiche_struct(const personne& p) 
{
	std::cout 
		<< "NOM : " 
		<< p.nom 
		<< ", age = " 
		<< p.age 
		<< "\n";
}
void affiche_tab(const personne* p, size_t n) 
{
	for (size_t i = 0; i < n; i++)
		affiche_struct(*p++);
}
void init_struct(personne& p, const char* n, unsigned int a) 
{
	p.age = a;
	char* pt = p.nom;
	while (*n) 
	{ 
		*pt = *n; 
		++pt; 
		++n; 
	}
	*pt = '\0';
}
void copy_struct(personne& dest, const personne& source) 
{
	dest = source; // En C++ l'op�rateur d'affectation est g�n�r� automatiquement.
}
void copy_tab(personne* dest, const personne* source, size_t n) 
{
	for (size_t i = 0; i < n; i++) 
		dest[i] = source[i]; //EN C++, l'op�rateur d'affectation est g�n�r� automatiquement.
}
void exercice_structure() {
	personne p1;
	affiche_struct(p1);
	raz(p1);
	affiche_struct(p1);
	personne p2 = { "Cooper", 28 };
	affiche_struct(p2);
	init_struct(p2, "Wolowitz", 27);
	affiche_struct(p2);
	copy_struct(p1, p2);
	affiche_struct(p2);
	personne tab1[3] = { "Cooper", 28, "Wolowitz", 27, "Hofstadter", 30 };
	personne tab2[3];
	copy_tab(tab2, tab1, 3);
	affiche_tab(tab2, 3);
}

int main()
{
	exercice_structure();
	return 0;
}