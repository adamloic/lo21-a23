#include "fonction.h"
#include <iostream>

int main()
{
	//double* pt0 = 0;//Marche, c'est �quivalent � dire que le pointeur pointe sur rien.
	double* pt0 = nullptr; //Meilleure fa�on de faire.
	//double* pt1 = 4096;//Pas de conversion de int vers double*. Vous pouvez pas rentrer d'adresse comme �a.
	double* pt2 = (double*)4096; //L� c'est possible, avec une conversion du C explicite. DANGEREUX = l'adresse m�moire est possiblement d�j� utilis�e.
	double* pt1 = reinterpret_cast<double*>(4096); //Conversion du C++, �viter le reinterpret cast autant que possible.
	//std::cout << *pt2; Ne marchera pas, car protection pour �viter que vous fassiez n'importe quoi avec des adresses m�moire prot�g�es.
	void* pt3 = pt1;//Conversion implicite de double* en void* possible.
	//pt1 = pt3;//Conversion implicite de void* en double* impossible.
	//pt1 = (double*)pt3;//Conversion explicite autoris�e, mais faites plut�t un static_cast.
	pt1 = static_cast<double*>(pt3);
	double d1 = 36;
	const double d2 = 36; //Constante.
	double* pt4 = &d1; //Aucun probl�me. Pointeur sur une variable. La valeur de pt4 est l'adresse m�moire de d1.
	const double* pt5 = &d1; //Pointeur qui ne permet pas de modifier l'objet point�.
	*pt4 = 2.1;
	//*pt5 = 2.1; //Pas autoris� de modifier pt5, comme nous avons const double*, pointeur qui ne permet pas de modifier un objet.
	//pt4 = &d2; //Pour manipuler une constante, il faut la garentie que le pointeur ne va pas modifier la constante. Or pt4 n'est pas un const double*.
	pt5 = &d2;
	double* const pt6 = &d1; //Pointeur qui ne peut pointer que sur une adresse m�moire.
	//pt6 = &d1;//Pas possible : pointeur qui ne peut pas changer de valeur.
	*pt6 = 2.78;
	//double* const pt6b = &d2; //Pas un pointeur permettant de manipuler des constantes.
	const double* const pt7 = &d1; //Pointeur qui pointe que sur une adresse, et qui ne modifier pas l'objet point�.
	//pt7 = &d1; //Changement d'adresse impossible.
	//*pt7 = 2.78; //Changement de valeur impossible.
	double const* pt8 = &d1; //Equivalent � const double*
	pt8 = &d2;
	pt8 = &d1;
	//*pt8 = 3.14; pt8 ne permet pas de modifier une variable.
	return 0;
}