#include "fonction.h"
#include <iostream>

//Dans le CPP ou le .h (pr�f�r�) les arguments par d�fauts, MAIS PAS LES DEUX !!!!!!
void init(point& pt, int _x, int _y, int _z) {
	pt.x = _x; pt.y = _y; pt.z = _z;
}
void essai_init() {
	point p;
	init(p);
	init(p, 1);
	init(p, 1, 2);
	init(p, 1, 2, 3);
}

int main()
{
	essai_init();
	return 0;
}