#include <QApplication>
#include <QWidget>
#include <QWindow>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    QWidget fenetre;
    fenetre.setFixedSize(250, 120);
    fenetre.setWindowTitle("Joueur");

    QLineEdit* nom = new QLineEdit(&fenetre); //La fenetre va gérer la vie des objets.
    QLabel* noml = new QLabel("Nom", &fenetre);
    QPushButton* ok = new QPushButton("Ok", &fenetre);

    nom->setFixedWidth(180); //Fixe la largeur

    //noml -> move(10,10); //Déplacer à la position 10,10.
    //nom -> mosve(60,10);

    QHBoxLayout* cnom = new QHBoxLayout; //nom et noml alignés sur un même axe horizontal.
    cnom -> addWidget(noml);
    cnom -> addWidget(nom);

    QVBoxLayout* couche = new QVBoxLayout; //cnom et ok alignés sur un même axe vertical.
    couche -> addLayout(cnom);
    couche -> addWidget(ok);
    fenetre.setLayout(couche);

    //ok -> setFixedWidth(230);
    //ok -> move(100,78);

    fenetre.show();
    return app.exec();
}
