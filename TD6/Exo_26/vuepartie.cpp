#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QProgressBar>
#include <QLCDNumber>
#include <QString>
#include <QMessageBox>
#include "vuecarte.h"
#include "vuepartie.h"

VuePartie::VuePartie(QWidget *parent) : QWidget(parent), vuecartes(20, nullptr)
{
    setWindowTitle("Set !");

    controleur.distribuer();

    //Barre de progression
    nbCartesPioche = new QProgressBar;
    nbCartesPioche->setRange(0, Set::Jeu::getInstance().getNbCartes());
    nbCartesPioche->setValue(controleur.getPioche().getNbCartes());
    nbCartesPioche->setFixedHeight(30);
    nbCartesPioche->setTextVisible(false); //Ne pas pas afficher %.

    //Score
    scoreJoueur = new QLCDNumber;
    scoreJoueur->display(0);
    scoreJoueur->setFixedHeight(30);

    //Labels
    pioche = new QLabel("Pioche");
    score = new QLabel("Score");

    //Layouts
    layoutInformations = new QHBoxLayout;
    layoutCartes = new QGridLayout;
    couche = new QVBoxLayout;

    //Layout information
    layoutInformations->addWidget(pioche);
    layoutInformations->addWidget(nbCartesPioche);
    layoutInformations->addWidget(score);
    layoutInformations->addWidget(scoreJoueur);

    //Créer vue cartes
    for(size_t i=0; i<20; i++)
        vuecartes[i] = new VueCarte;
    for(size_t i=0; i<20; i++)
    {
        layoutCartes -> addWidget(vuecartes[i],i/4,i%4);
        connect(vuecartes[i],SIGNAL(carteClicked(VueCarte*)),this,SLOT(carteClique(VueCarte*)));
    }
    //Affectation des cartes actuelles
    size_t j = 0;
    for(auto it = controleur.getPlateau().begin(); it != controleur.getPlateau().end(); ++it)
        vuecartes[j++]->setCarte(*it);

    couche->addLayout(layoutInformations);
    couche->addLayout(layoutCartes);
    setLayout(couche);
}

void VuePartie::carteClique(VueCarte* vc)
{
    if(!vc ->cartePresente())
    {
        //qDebug("Ajouter une carte...");
        if(controleur.getPioche().getNbCartes()==0)
        {
            QMessageBox message(QMessageBox::Icon::Warning, "Attention", "La pioche est vide !!!");
            message.exec();
        }
        controleur.distribuer(); //Ajout cartes.
        size_t j = 0;
        for(auto it = controleur.getPlateau().begin(); it != controleur.getPlateau().end(); ++it)
            vuecartes[j++]->setCarte(*it);
        nbCartesPioche->setValue(controleur.getPioche().getNbCartes());
    }
    else
    {
        if(vc->isChecked()) //Si une carte est sélectionnée.
        {
            selectionCartes.insert(&vc->getCarte()); //Insérer la carte dans les cartes sélectionnées.
            if(selectionCartes.size() == 3) //Si 3 : vérifier si combinaison.
            {
                vector<const Set::Carte*> c(selectionCartes.begin(), selectionCartes.end()); //On rajoute les cartes via l'itérateur.
                Set::Combinaison comb(*c[0], *c[1], *c[2]);
                if(comb.estUnSET()) //Si c'est un set : il faut mettre à jour le plateau.
                {
                    controleur.getPlateau().retirer(*c[0]);
                    controleur.getPlateau().retirer(*c[1]);
                    controleur.getPlateau().retirer(*c[2]);
                    //S'il n'y a pas 12 cartes : en piocher.
                    if(controleur.getPlateau().getNbCartes()< 12)
                        controleur.distribuer();
                    //Mettre à jour le score.
                    scoreValue++;
                    scoreJoueur->display(scoreValue);
                    //Mettre à jour la vue des carte.
                    for(size_t i=0; i<vuecartes.size(); i++)
                        vuecartes[i]->setNoCarte();
                    size_t j = 0;
                    for(auto it = controleur.getPlateau().begin(); it != controleur.getPlateau().end(); ++it)
                        vuecartes[j++]->setCarte(*it);
                    //Mettre à jour la vue de la pioche
                    nbCartesPioche->setValue(controleur.getPioche().getNbCartes());
                }
                else
                {
                    QMessageBox message(QMessageBox::Icon::Warning, "Attention", "Ce n'est pas un set !!!");
                    message.exec();
                    //Déselectionner les cartes
                    for(size_t i=0; i<vuecartes.size(); i++)
                        vuecartes[i]->setChecked(false);
                    selectionCartes.clear(); //On vide le vecteur.
                }
            }
        }
        else
        {
            //Déselectionner la carte.
            selectionCartes.erase(&vc->getCarte());
        }
    }


    update();
}
