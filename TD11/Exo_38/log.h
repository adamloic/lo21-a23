#if !defined(LOG_H)
#define LOG_H
#include "timing.h"
#include "evenement.h"
#include<iostream>

class Log 
{
public:
	virtual void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) = 0;
	virtual void displayLog(std::ostream& f) const = 0;
};

//Adapateur de classe (double h�ritage dont un priv�)
class MyLog : public Log, private TIME::Agenda
{
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) override;
	void displayLog(std::ostream& f) const override;
};

//Adaptateur d'objet (agenda en attribut pour l'utiliser)
class MyLog2 : public Log
{
private:
	TIME::Agenda evts;
public:
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) override;
	void displayLog(std::ostream& f) const override;
};

class LogError : public std::exception
{
	std::string info;
public:
	LogError(const char* s) noexcept : info(s) {}
	const char* what() const noexcept { return info.c_str(); }
};

#endif