#include "evenement.h"

TIME::Agenda& TIME::Agenda::operator << (const Evt& e)
{
	tab.push_back(e.clone());
	return *this;
}

void TIME::Agenda::afficher(std::ostream& f) const
{
	f << "##### AGENDA #####\n";
	for (auto it = tab.begin(); it != tab.end(); it++)
		(*(*it)).afficher();
	f << "##### AGENDA #####\n";
}
