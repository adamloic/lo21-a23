#include "log.h"

void MyLog::addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s)
{
	*this << TIME::Evt1jDur(d, s, h, TIME::Duree(0)); //En fait on a
}

void MyLog::displayLog(std::ostream& f) const
{
	for (Agenda::const_iterator it = begin(); it != end(); ++it)
	{
		f << dynamic_cast<const TIME::Evt1jDur&>(*it).getDate() << "-";
		f << dynamic_cast<const TIME::Evt1jDur&>(*it).getHoraire() << ":";
		f << (*it).getDescription() << "\n";
	} //on devrait v�rifier que les conversions marchent.
}

void MyLog2::addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s)
{
	evts << TIME::Evt1jDur(d, s, h, TIME::Duree(0)); //En fait on a
}

void MyLog2::displayLog(std::ostream& f) const
{
	for (auto it = evts.begin(); it != evts.end(); ++it)
	{
		f << dynamic_cast<const TIME::Evt1jDur&>(*it).getDate() << "-";
		f << dynamic_cast<const TIME::Evt1jDur&>(*it).getHoraire() << ":";
		f << (*it).getDescription() << "\n";
	} //on devrait v�rifier que les conversions marchent.


}