#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H

#include <iostream>
#include <string>
#include <vector>
#include "timing.h"

namespace TIME
{
	class Evt
	{
    private:
        std::string sujet;
    public:
        Evt(const std::string& s) : sujet(s) {}
        const std::string& getDescription() const {return sujet;}
        virtual void afficher(std::ostream& f = std::cout) const = 0;
	};

	class Evt1j : public Evt
	{
	private:
		Date date;
	public:
		Evt1j(const Date& d, const std::string& s) : Evt(s), date(d) { std::cout << "Evt1j \n"; }
		virtual ~Evt1j() { std::cout << "Bye Evt1j \n"; }
		const Date& getDate() const { return date; }
		void afficher(std::ostream& f = std::cout) const
		{
			f << "***** Evt ********" << "\n" << "Date=" << date << " sujet=" << getDescription() << "\n";
		}
	};

	class Evt1jDur : public Evt1j
	{
	private:
		Horaire debut;
		Duree duree;
	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& t) :
			Evt1j(d, s), debut(h), duree(t) { std::cout << "Evt1jDur \n";}
		~Evt1jDur() { std::cout << "Bye Evt1jDur \n"; }
		const Horaire& getHoraire() const { return debut; }
		const Duree& getDuree() const { return duree; }
		void afficher(std::ostream& f = std::cout) const
		{
			Evt1j::afficher(f);
			f << "debut =" << debut << " duree=" << duree << "\n";
		}
	};

	class Rdv : public Evt1jDur
	{
	private:
		std::string personne;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s, const Horaire& h, const Duree& t,
			const std::string& p, const std::string& l) : Evt1jDur(d, s, h, t),
			personne(p), lieu(l) { std::cout << "Rdv \n"; }
		~Rdv() { std::cout << "Bye Rdv \n"; }
		const std::string& getPersonne() const { return personne; }
		const std::string& getLieu() const { return lieu; }
		void afficher(std::ostream& f = std::cout) const
		{
			Evt1jDur::afficher(f);
			f << " personne =" << personne << " lieu=" << lieu << "\n";
		}
	};

	class EvtPj : public Evt
	{
    private:
        Date debut;
        Date fin;
    public:
        EvtPj(const Date& d, const Date& f, const std::string& s):
            Evt(s), debut(d), fin(f) {};
        const Date& getDateDebut() const {return debut;}
        const Date& getDateFin() const {return fin;}
        void afficher(std::ostream& f = std::cout) const
		{
			f << "***** Evt ********" << "\n";
			f << "Date debut = " << debut;
			f << " date fin = " << fin << "\n";
			f << " sujet = " << getDescription() << "\n";
		}
	};

	class Agenda
	{
    private:
        std::vector<Evt*> tab;
    public:
        Agenda() = default; //Tab va �tre initi� par le constructeur par defaut.
        virtual ~Agenda() = default;
        Agenda(const Agenda&) = delete;
        Agenda& operator=(const Agenda&) = delete;
        Agenda& operator<<(Evt& e);
        void afficher(std::ostream& f = std::cout) const;
	};

}
#endif
