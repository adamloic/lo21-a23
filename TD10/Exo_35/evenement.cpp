#include "evenement.h"

void TIME::Agenda::operator<<(const TIME::Evt& e)
{
    tab.push_back(e.clone());
}

void TIME::Agenda::afficher(std::ostream& f) const
{
    f << "##### AGENDA #####" << "\n";
    for(unsigned int i = 0; i< tab.size(); i++)
        tab[i] -> afficher(f);
    f << "##### AGENDA #####" << "\n";
}
