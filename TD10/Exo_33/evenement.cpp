#include "evenement.h"

TIME::Agenda& TIME::Agenda::operator<<(Evt1j& e)
{
    tab.push_back(&e);
    return *this;
}

void TIME::Agenda::afficher(std::ostream& f) const
{
    f << "##### AGENDA #####" << "\n";
    for(unsigned int i = 0; i< tab.size(); i++)
        tab[i] -> afficher(f);
    f << "##### AGENDA #####" << "\n";
}
