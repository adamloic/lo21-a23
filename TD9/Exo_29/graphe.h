#if !defined(_GRAPH_H)
#define _GRAPH_H

#include<string>
#include<stdexcept>
#include<list>
#include<vector>
#include<iostream>
#include<algorithm>

class GraphException : public std::exception 
{
	std::string info;
public:
	GraphException(const std::string& i) noexcept :info(i) {}
	virtual ~GraphException() noexcept {}
	const char* what() const noexcept {  return info.c_str(); }
};

class Graph {
	std::vector<std::list<unsigned int> > adj;
	std::string name;
	unsigned int nb_edges;
public:
	Graph(const std::string& n, size_t nb) : adj(nb), name(n), nb_edges(0) {}
	const std::string& getName() const { return name; }
	size_t getNbVertices() const { return adj.size(); }
	size_t getNbEdges() const { return nb_edges; }
	void addEdge(unsigned int i, unsigned int j);
	void removeEdge(unsigned int i, unsigned int j);
	const std::list<unsigned int>& getSuccessors(unsigned int i) const;
	const std::list<unsigned int> getPredecessors(unsigned int i) const;
};

std::ostream& operator<<(std::ostream& f, const Graph& G);

#endif
