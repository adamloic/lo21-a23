#include "graphe.h"

void Graph::addEdge(unsigned int i, unsigned int j)
{
	if (i >= getNbVertices() || j >= getNbVertices())
		throw GraphException("Erreur dans les arguments : le sommet n'existe pas.");
	if (find(adj[i].begin(), adj[i].end(), j)!= adj[i].end())
		throw GraphException("Sommet d�j� ajout� dans la liste des successeurs.");
	//adj[i].push_back(j);
	adj[i].insert(adj[i].end(), j); //Ceci marchera pour d'autres conteneurs !
	nb_edges++;
}
void Graph::removeEdge(unsigned int i, unsigned int j)
{
	if (i >= getNbVertices() || j >= getNbVertices())
		throw GraphException("Erreur dans les arguments : le sommet n'existe pas.");
	std::list<unsigned int>::iterator it = find(adj[i].begin(), adj[i].end(), j);
	if(it != adj[i].end())
	{
		adj[i].erase(it);
		nb_edges--;
	}
	else
		throw GraphException("Sommet non pr�sent dans la liste des successeurs.");
}
const std::list<unsigned int>& Graph::getSuccessors(unsigned int i) const
{
	if (i >= getNbVertices())
		throw GraphException("Erreur dans les arguments : le sommet n'existe pas.");
	return adj[i];
}
const std::list<unsigned int> Graph::getPredecessors(unsigned int j) const
{
	if (j >= getNbVertices())
		throw GraphException("Erreur dans les arguments : le sommet n'existe pas.");
	std::list<unsigned int> pred;
	for (unsigned int i = 0; i < adj.size(); i++)
	{
		if (find(adj[i].begin(), adj[i].end(), j) != adj[i].end())
			pred.insert(pred.end(), i);
	}
	return pred;
}

std::ostream& operator<<(std::ostream& f, const Graph& G)
{
	f << "Graph " << G.getName() << " (" << G.getNbVertices()
		<< " vertices and " << G.getNbEdges() << " edges)" << "\n";

	for (size_t i = 0; i < G.getNbVertices(); i++) 
	{
		f << i << ":";
		for (std::list<unsigned int>::const_iterator it = G.getSuccessors(i).begin(); 
			it != G.getSuccessors(i).end(); ++it) 
			f << *it << " ";
		f << "\n";
	}

	f << "\n";

	for (size_t i = 0; i < G.getNbVertices(); i++)
	{
		f << i << ":";
		auto pred = G.getPredecessors(i);
		for (std::list<unsigned int>::const_iterator it = pred.begin(); it != pred.end(); ++it)
			f << *it << " ";
		f << "\n";
	} 
	return f;
}

