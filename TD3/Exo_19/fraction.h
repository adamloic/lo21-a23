#if !defined(_FRACTION_H)
#define _FRACTION_H
#include <iostream>

namespace math
{
	class Fraction
	{
	private:
		int numerateur;
		int denominateur;
		void simplification();

	public:
		int getNumerateur() const { return numerateur; } //const car on ne modifie pas l'objet.
		int getDenominateur() const { return denominateur;  }
		void setFraction(int n, int d);
		Fraction(int n = 0, int d = 1) { setFraction(n, d); std::cout << "Premiere bouffee d'air de " << this << "\n";} //On affecte.
		/*Fraction(int n, int d) : numerateur(n), denominateur() { simplification(); } //Ils sont initialis�s.
		Fraction(int n) : numerateur(n), denominateur(1) {} //Ils sont initialis�s.
		Fraction() : numerateur(0), denominateur(1) {}  //Ils sont initialis�s.*/
		const Fraction somme(const Fraction& f) const; //On ne modifie pas la classe.
		//Mettre un retour const =/= object constant. Juste dire qu'on a pas le droit d'utiliser le resultat temporaire en tant que lvalue. Par exemple (somme(somme(f1, f2), f3)
		~Fraction() { std::cout << "Adieu objet tant adore qui va nous manquer " << this << "\n"; }
	};

	const Fraction somme(const Fraction& f1, const Fraction& f2); //Le const au d�but (et aussi dans l'autre fonction au d�but) sont obsel�tes, mais on garde quand m�me.
		
}

#endif