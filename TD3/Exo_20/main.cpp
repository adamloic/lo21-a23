#include <iostream>
#include "fraction.h"
using namespace std;
using namespace math;

int main() 
{
	Fraction f1(3, 4);
	Fraction f2(1, 6);
	
	Fraction f3 = 5+f1;
	++f3;
	f3++;

	std::cout << f1;
	return 0;
}