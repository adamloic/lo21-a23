#if !defined(_FRACTION_H)
#define _FRACTION_H
#include <iostream>

namespace math
{
	class Fraction
	{
	private:
		int numerateur;
		int denominateur;
		void simplification();

	public:
		int getNumerateur() const { return numerateur; } //const car on ne modifie pas l'objet.
		int getDenominateur() const { return denominateur;  }
		void setFraction(int n, int d);
		Fraction(int n = 0, int d = 1) { setFraction(n, d);} //On affecte.
		/*Fraction(int n, int d) : numerateur(n), denominateur() { simplification(); } //Ils sont initialis�s.
		Fraction(int n) : numerateur(n), denominateur(1) {} //Ils sont initialis�s.
		Fraction() : numerateur(0), denominateur(1) {}  //Ils sont initialis�s.*/
		const Fraction somme(const Fraction& f) const; //On ne modifie pas la classe.
		//Mettre un retour const =/= object constant. Juste dire qu'on a pas le droit d'utiliser le resultat temporaire en tant que lvalue. Par exemple (somme(somme(f1, f2), f3)
		~Fraction() = default; //On utilise le d�structeur par d�faut.

		//const Fraction operator+(const Fraction& f) const {return somme(f); } //Ne permet pas de faire 5+f1
		Fraction& operator++(); //version pr�fixe
		Fraction operator++(int); //version postfixe
	};

	const Fraction somme(const Fraction& f1, const Fraction& f2); //Le const au d�but (et aussi dans l'autre fonction au d�but) sont obsel�tes, mais on garde quand m�me.
	const Fraction operator+(const Fraction& f1, const Fraction& f2); //Permet de faire 5+f1.
	std::ostream& operator<<(std::ostream & F, const Fraction& frac);
}

#endif