#include "fraction.h"
#include <iostream>

void math::Fraction::setFraction(int n, int d)
{
	numerateur = n;
	if (d == 0)
	{
		//throw "Erreur : d�nominateur nul";
		throw FractionException("Erreur : d�nominateur nul");
		denominateur = 1;
	}
	denominateur = d;
	simplification();
}

const math::Fraction math::Fraction::somme(const math::Fraction& f) const
{
	return Fraction(numerateur * f.denominateur + f.numerateur * denominateur, denominateur * f.denominateur);
}

//On ne peut pas faire appel aux m�thodes et attributs priv�s, car nous sommes en dehors de la classe Fraction.
const math::Fraction math::somme(const Fraction& f1, const Fraction& f2)
{
	return Fraction(f1.getNumerateur() * f2.getDenominateur() + f2.getNumerateur() * f1.getDenominateur(), f1.getDenominateur() * f2.getDenominateur());
}

const math::Fraction math::operator+(const Fraction& f1, const Fraction& f2)
{
	return math::somme(f1, f2);
}

math::Fraction& math::Fraction::operator++()//pr�fixe
{
	setFraction(numerateur + denominateur, denominateur);
	return *this;
}
math::Fraction math::Fraction::operator++(int)//postfixe
{
	Fraction f(numerateur, denominateur);//copie
	setFraction(numerateur + denominateur, denominateur);
	return f;
}

std::ostream& math::operator<<(std::ostream& F, const math::Fraction& frac)
{
	F << frac.getNumerateur();
	if (frac.getDenominateur() != 1)
		F << "/" << frac.getDenominateur();
	return F;
}

// � ajouter en tant m�thode priv�e de la classe Fraction
void math::Fraction::simplification() {
	// si le numerateur est 0, le denominateur prend la valeur 1
	if (numerateur == 0) { denominateur = 1; return; }
	/* un denominateur ne devrait pas �tre 0;
	si c�est le cas, on sort de la m�thode */
	if (denominateur == 0) return;
	/* utilisation de l�algorithme d�Euclide pour trouver le Plus Grand Commun
	Denominateur (PGCD) entre le numerateur et le denominateur */
	int a = numerateur, b = denominateur;
	// on ne travaille qu�avec des valeurs positives...
	if (a < 0) a = -a; if (b < 0) b = -b;
	if (denominateur == 1) return;
	while (a != b) { if (a > b) a = a - b; else b = b - a; }
	// on divise le numerateur et le denominateur par le PGCD=a
	numerateur /= a; denominateur /= a;
	// si le denominateur est n�gatif, on fait passer le signe - au denominateur
	if (denominateur < 0) { denominateur = -denominateur; numerateur = -numerateur; }
}
