#include <iostream>
#include "fraction.h"
using namespace std;
using namespace math;

int main() 
{
	try
	{
		Fraction A(1, 2);
		Fraction B(5, 0);
	}
	catch(const char* e)
	{
		std::cout << e << "\n";
	}
	catch (FractionException& e)
	{
		std::cout << e.getInfo() << "\n";
	}
	return 0;
}