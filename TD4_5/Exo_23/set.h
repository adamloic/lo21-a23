#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
#include <vector>
using namespace std;

namespace Set {
	// classe pour g�rer les exceptions dans le set
	class SetException 
	{
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caract�ristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un=1, deux=2, trois=3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caract�ristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// �criture d'une caract�ristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caract�ristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caract�ristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);

	class Carte
	{
	private:
		Couleur couleur;
		Nombre nombre;
		Forme forme;
		Remplissage remplissage;
		Carte(Couleur c, Nombre n, Forme f, Remplissage r) : couleur(c), nombre(n), forme(f), remplissage(r) {}
		friend class Jeu;
	public:
		Couleur getCouleur() const { return couleur; }
		Nombre getNombre() const { return nombre; }
		Forme getForme() const { return forme; }
		Remplissage getRemplissage() const { return remplissage; }
		~Carte() = default; //On utilise par defaut.
		Carte(const Carte& c) = default; //On utilise par defaut.
		Carte& operator=(const Carte& c) = default; //On utilise par defaut.
 	};

	ostream& operator<< (ostream& f, const Carte& c);

	class Jeu
	{
	private:
		//const Carte* cartes[81]; //Ne pas oublier de faire Jeu* Jeu::instance = nullptr dans le cpp 
		std::array<const Carte*, 81> cartes;
		Jeu();
		~Jeu();
		Jeu(const Jeu& j) = delete;
		Jeu& operator=(const Jeu& j) = delete;

		//static Jeu* instance;
		struct Handler 
		{
			Jeu* instance;
			Handler() :instance(nullptr) {} //Pointeur initilis� � la cr�ation de l'Handler
			~Handler() { delete instance; } //Lib�ration � la d�struction du Handler. 
		};
		static Handler handler;
	public:
		size_t getNbCartes() const { return 81; } //size_t getNbCartes() const { return cartes.size(); }

		/*static Jeu& getInstance()//Methode 1 : pas de gestion de la m�moire � faire, mais une fois l'objet jeu cr�e, il est pas possible de le supprimer et d'en refaire un nouveau.
		{
			static Jeu jeu; // unique instance
			return jeu; // c�est toujours le m�me objet retourn�
		}*/

		/*static Jeu& getInstance() //M�thode 2 : gestion de la m�moire, mais possible de supprimer le jeu et d'en refaire un nouveau.
		{ 
			if (instance == nullptr) 
				instance = new Jeu;
			return *instance;
		}
		static void libererInstance() 
		{ 
			delete instance; 
			instance = nullptr; 
		}*/


		static Jeu& getInstance()
		{
			if (handler.instance == nullptr) //M�thode 3 : comme la m�thode avec attribut, mais en plus : � la lib�ration de Handler, on est s�r que l'instance est lib�r�e.
				handler.instance = new Jeu;
			return *handler.instance;
		}
		static void libererInstance()
		{
			delete handler.instance;
			handler.instance = nullptr;
		}

		class Iterator 
		{
		public:
			void next() 
			{
				if (isDone()) 
					throw SetException("Iterateur en fin de sequence"); 
				i++; 
			}
			bool isDone() const { return i == Jeu::getInstance().getNbCartes(); }
			const Carte& currentItem() const 
			{
				if (isDone()) 
					throw SetException("Iterateur en fin de sequence");
				return Jeu::getInstance().getCarte(i);
			}
		private:
			size_t i = 0;
			friend class Jeu;
			Iterator() = default; // seule la classe Jeu peut construire un Iterator
			};

		class FormeIterator
		{
		public:
			void next()
			{
				if (isDone())
					throw SetException("Iterateur en fin de sequence");
				i++;
				while (!isDone() && Jeu::getInstance().getCarte(i).getForme() != forme)
					i++;
			}
			bool isDone() const { return i == Jeu::getInstance().getNbCartes(); }
			const Carte& currentItem() const
			{
				if (isDone())
					throw SetException("Iterateur en fin de sequence");
				return Jeu::getInstance().getCarte(i);
			}
		private:
			size_t i = 0;
			friend class Jeu;
			Forme forme;
			FormeIterator(Forme f) :forme(f)
			{
				while (!isDone() && (Jeu::getInstance().getCarte(i).getForme() != forme))
				{
					i++;
				}
			}
		};

		Iterator getIterator() const { return Iterator(); }
		FormeIterator getIterator(Forme f) const { return FormeIterator(f); }
		friend class Iterator;
		friend class FormeIterator;
		const Carte& getCarte(size_t i) const;
	};

	class Pioche
	{
	private:
		const Carte** cartes = nullptr; //std::vector<Carte*> cartes;
		size_t nb = 0;
	public:
		explicit Pioche(); //Pour �viter que Jeu soit converti en autre chose (genre Pioche)
		bool estVide() const { return nb == 0; }
		size_t getNbCartes() const { return nb; }
		const Carte& piocher();
		~Pioche();
		Pioche(const Pioche& p) = delete;
		Pioche& operator= (const Pioche& p) = delete;
	};

	class Plateau
	{
	private:
		const Carte** cartes = nullptr;
		//std::vector<const Carte*> cartes;
		size_t nbMax = 0;
		size_t nb = 0;
	public:
		Plateau() = default;
		~Plateau() { delete[] cartes; }
		size_t getNbCartes() const { return nb; }
		void ajouter(const Carte& c);
		void retirer(const Carte& c);
		void print(ostream& f = cout) const;
		Plateau(const Plateau& p);
		Plateau& operator= (const Plateau& p);
		class const_iterator {
		public:
			const_iterator& operator++() { current++; return *this; }
			const Carte& operator*() const { return **current; }
			bool operator!=(const_iterator it) const { return current != it.current; }
		private:
			const_iterator(const Carte** c) :current(c) {}
			friend class Plateau;
			const Carte** current = nullptr;
		};
		const_iterator begin() const { return const_iterator(cartes); }
		const_iterator end() const { return const_iterator(cartes + nb); }
	};

	ostream& operator << (ostream& f, const Plateau& p);

	class Combinaison
	{
	private:
		const Carte* c1;
		const Carte* c2;
		const Carte* c3;
	public:
		Combinaison(const Carte* u, const Carte* v, const Carte* w): c1(u), c2(v), c3(w){}
		bool estUnSET() const;
		const Carte& getCarte1() const { return *c1; }
		const Carte& getCarte2() const { return *c2; }
		const Carte& getCarte3() const { return *c3; }
		~Combinaison() = default;
		Combinaison(const Combinaison& c) = default;
		Combinaison& operator= (const Combinaison& c) = default;
	};

	ostream& operator << (ostream& f, const Combinaison& c);

	class Controleur
	{
	private:
		Pioche* pioche = nullptr;
		Plateau plateau;
	public:
		Controleur() { pioche = new Pioche; }
		~Controleur() { delete pioche; }
		void distribuer();
		const Plateau& getPlateau() const { return plateau; }
	};
}


#endif