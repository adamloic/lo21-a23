#include "set.h"
using namespace Set;

#include <array>

/*int main()
{
	try 
	{
		printCouleurs();
		printNombres();
		printFormes();
		printRemplissages();

		Carte c = Carte(Couleur::rouge, Nombre::un, Forme::ovale, Remplissage::vide);
		//std::cout << c << "\n";
		Jeu j;
		Pioche p(j);
		std::cout << p.piocher() << "\n";
		std::cout << p.piocher() << "\n";

		Plateau m;
		m.ajouter(Carte(Couleur::rouge, Nombre::un, Forme::ovale, Remplissage::vide));
		m.ajouter(Carte(Couleur::vert, Nombre::deux, Forme::ovale, Remplissage::vide));
		std::cout << m;
	}
	catch (SetException& e) 
	{
		std::cout << e.getInfo() << "\n";
	}
	system("pause");
	return 0;
}*/

#include "set.h"
using namespace Set;
int main() {
	try {
		Controleur c;
		c.distribuer();
		cout << c.getPlateau();
		c.distribuer();
		cout << c.getPlateau();
	}
	catch (SetException& e) {
		std::cout << e.getInfo() << "\n";
	}
	return 0;
}