#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
#include <vector>
using namespace std;

namespace Set {
	// classe pour gérer les exceptions dans le set
	class SetException 
	{
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caractéristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un=1, deux=2, trois=3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caractéristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// écriture d'une caractéristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caractéristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caractéristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);

	class Carte
	{
	private:
		Couleur couleur;
		Nombre nombre;
		Forme forme;
		Remplissage remplissage;
	public:
		Couleur getCouleur() const { return couleur; }
		Nombre getNombre() const { return nombre; }
		Forme getForme() const { return forme; }
		Remplissage getRemplissage() const { return remplissage; }
		Carte(Couleur c, Nombre n, Forme f, Remplissage r) : couleur(c), nombre(n), forme(f), remplissage(r) {}
		~Carte() = default; //On utilise par defaut.
		Carte(const Carte& c) = default; //On utilise par defaut.
		Carte& operator=(const Carte& c) = default; //On utilise par defaut.
 	};

	ostream& operator<< (ostream& f, const Carte& c);

	class Jeu
	{
	private:
		//const Carte* cartes[81]; 
		std::array<const Carte*, 81> cartes;
	public:
		size_t getNbCartes() const { return 81; } //size_t getNbCartes() const { return cartes.size(); }
		const Carte& getCarte(size_t i) const;
		Jeu();
		~Jeu();
		Jeu(const Jeu& j) = delete;
		Jeu& operator=(const Jeu& j) = delete;
	};

	class Pioche
	{
	private:
		const Carte** cartes = nullptr; //std::vector<Carte*> cartes;
		size_t nb = 0;
	public:
		explicit Pioche(const Jeu& j); //Pour éviter que Jeu soit converti en autre chose (genre Pioche)
		bool estVide() const { return nb == 0; }
		size_t getNbCartes() const { return nb; }
		const Carte& piocher();
		~Pioche();
		Pioche(const Pioche& p) = delete;
		Pioche& operator= (const Pioche& p) = delete;
	};

	class Plateau
	{
	private:
		const Carte** cartes = nullptr;
		//std::vector<const Carte*> cartes;
		size_t nbMax = 0;
		size_t nb = 0;
	public:
		Plateau() = default;
		~Plateau() { delete[] cartes; }
		size_t getNbCartes() const { return nb; }
		void ajouter(const Carte& c);
		void retirer(const Carte& c);
		void print(ostream& f = cout) const;
		Plateau(const Plateau& p);
		Plateau& operator= (const Plateau& p);
	};

	ostream& operator << (ostream& f, const Plateau& p);

	class Combinaison
	{
	private:
		const Carte* c1;
		const Carte* c2;
		const Carte* c3;
	public:
		Combinaison(const Carte* u, const Carte* v, const Carte* w): c1(u), c2(v), c3(w){}
		bool estUnSET() const;
		const Carte& getCarte1() const { return *c1; }
		const Carte& getCarte2() const { return *c2; }
		const Carte& getCarte3() const { return *c3; }
		~Combinaison() = default;
		Combinaison(const Combinaison& c) = default;
		Combinaison& operator= (const Combinaison& c) = default;
	};

	ostream& operator << (ostream& f, const Combinaison& c);

	class Controleur
	{
	private:
		Jeu jeu;
		Pioche* pioche = nullptr;
		Plateau plateau;
	public:
		Controleur() { pioche = new Pioche(jeu); }
		~Controleur() { delete pioche; }
		void distribuer();
		const Plateau& getPlateau() const { return plateau; }
	};
}


#endif