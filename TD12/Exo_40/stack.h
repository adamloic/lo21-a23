#if !defined(_Stack_T_H)
#define _Stack_T_H

#include "container.h"
#include "vector.h"

namespace TD
{
	namespace AO
	{
		template<typename T, typename CONT = Vector<T> >
		class Stack
		{
			CONT cont;
		public:
			Stack() { CONT(); }
			void push(const T& x) { cont.push_back(x); }
			void pop() { cont.pop_back(); }
			const T& top() const { return cont.back(); }
			T& top() { return cont.back(); }
			bool empty() const { return cont.empty(); }
			void clear() { cont.clear(); }
		};
	}

	namespace AC
	{
		template<typename T, typename CONT = Vector<T>>
		class Stack : private CONT
		{
		public:
			Stack() : CONT(T()) {}
			void push(const T& x) { CONT::push_back(x); }
			void pop() { CONT::pop_back(); }
			const T& top() const { return CONT::back(); }
			T& top() { return CONT::back(); }
			using CONT::empty;
			using CONT::clear;
		};
	}
}

#endif
