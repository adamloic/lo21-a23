#include "container.h"
#include "vector.h"
#include <iostream>
#include <vector>
#include "stack.h"

int main()
{
	auto v = TD::Vector<int>();

	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	std::cout << v[0] << "\n";
	std::cout << v[4] << "\n";
	
	auto s = TD::AO::Stack<int>();
	//auto s = TD::AO::Stack<int, TD::Vector<int>>();

	s.push(1);
	s.push(2);
	std::cout << s.top() << "\n";


	return 0;
}