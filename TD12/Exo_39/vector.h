#if !defined(_Vector_T_H)
#define _Vector_T_H

#include "container.h"

namespace TD
{
	template<typename T>
	class Vector : public Container<T>
	{
	private:
		T* tab;
		size_t cap; //Taille max du tableau, cr�er un nouveau si nbEl = cap
	public:
		Vector(const T& initialize_with = T());
		Vector(const Vector<T>&);
		~Vector() { delete[] tab; }
		Vector<T>& operator= (const Vector<T>& t);
		T& element(size_t i) override;
		const T& element(size_t i) const override;
		T& operator[](size_t i);
		const T& operator[](size_t i) const;
		void push_back(const T& x) override;
		void pop_back() override;
	};
}

template<class T> TD::Vector<T>::Vector(const T& initialize_with): 
	Container<T>(), tab(new T[4]), cap(4)
{
	for (size_t i = 0; i < this->nbEl ; i++) //Ou Container<T>::nbEl.
		tab[i] = initialize_with;
}

template<class T> TD::Vector<T>::Vector(const Vector<T>& t) :
	Container<T>(), tab(new T[t.size()]), cap(t.size())
{
	for (size_t i = 0; i < t.size(); i++) //Ou Container<T>::nbEl.
		tab[i] = t.tab[i];
}

template<class T> TD::Vector<T>& TD::Vector<T>::operator=(const TD::Vector<T>& t)
{
	if (this != &t)
	{
		T* newtab = new T[t.nbEl];
		for (size_t i = 0; i < t.nbEl; i++)
			newtab[i] = t.tab[i];
		this->nbEl = t.nbEl;
		cap = t.nbEl;
		T* old = tab;
		tab = newtab;
		delete[] old;
	}
	return *this;
}

template<class T> T& TD::Vector<T>::element(size_t i)
{
	if (!this->empty())
		return tab[i];
	throw ContainerException("Erreur : le conteneur est vide");
}

template<class T> const T& TD::Vector<T>::element(size_t i) const
{
	if (!this->empty())
		return tab[i];
	throw ContainerException("Erreur : le conteneur est vide");
}

template<class T> T& TD::Vector<T>::operator[](size_t i)
{
	return element(i);
}

template<class T> const T& TD::Vector<T>::operator[](size_t i) const
{
	return element(i);
}

template<class T> void TD::Vector<T>::push_back(const T& x)
{
	if (this->nbEl == cap)
	{
		T* newtab = new T[2*cap];
		for (size_t i = 0; i < this->nbEl; i++)
			newtab[i] = tab[i];
		cap *= 2;
		T* old = tab;
		tab = newtab;
		delete[] old;
	}
	tab[this->nbEl++] = x;
}

template<class T> void TD::Vector<T>::pop_back()
{
	if (!this->empty())
		this->nbEl--;
	throw ContainerException("Erreur : le conteneur est vide");
}

#endif