#include "fonction.h"

void exercice_surcharge() {
	
	int i = 3, j = 15;
	float x = 3.14159, y = 1.414;
	char c = 'A';
	double z = 3.14159265;

	fct(i); //1:3
	fct(x); //2:3.14159
	fct(i, y); //3:31.414
	fct(x, j); //4:3.1415915
	fct(c); //1:65
	fct(static_cast<int>(c)); //Conversion explicit du C++ (ne pas utiliser celle du C).
	//fct(i, j); //Ne devrait pas marcher : on a pas fct(int int) et int -> float existe, et on a le choix entre int, float et float, int => ambiguit� le compilateur aime pas.
	fct(static_cast<float>(i), j); //l� ca marche, car gr�ce � la conversion explicite, on sait quelle surchage prendre.
	//fct(i, c); //Ne devrait pas marcher : char -> int implicitement et m�me probl�me qu'avant.
	fct(static_cast<float>(i), static_cast<int>(c));
	fct(i, z); //3:3 et quelque chose, car double -> float implicitement possible, mais attention, si le nombre est plus grand que la borne sup du float, convertion approximation.
	//fct(z, z); //Double -> float, mais pas double -> int
	fct(static_cast<int>(4), z); //Explicite double -> int possible, mais tronque.
}

int main()
{
	exercice_surcharge();
	return 0;
}