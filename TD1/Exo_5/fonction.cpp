#include <iostream>
//using namespace std; //Atention s'il y a d'autres fonctions ayant le m�me nom que dans le namespace std.
const double PI = 3.14159;
void exerciceA() {
	std::cout << "donnez le rayon entier d�un cercle:";
	int r;
	std::cin >> r;
	double p = 2 * PI * r;
	double s = PI * r * r;
	std::cout << "le cercle de rayon " << r << " ";
	std::cout << "a un perimetre de " << p << " et une surface de " << s << "\n";
}

int main()
{
	exerciceA();
	return 0;
}